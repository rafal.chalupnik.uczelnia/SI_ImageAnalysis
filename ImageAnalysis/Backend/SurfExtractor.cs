﻿using Accord.Imaging;
using System.Collections.Generic;
using System.IO;

namespace Backend
{
    public class SurfExtractor
    {
        public IEnumerable<SpeededUpRobustFeaturePoint> ExtractPoints(string _filePath)
        {
            if (string.IsNullOrEmpty(_filePath) || !File.Exists(_filePath))
                return null;

            var bitmap = Image.FromFile(_filePath);

            var surf = new SpeededUpRobustFeaturesDetector();
            return surf.Transform(bitmap);
        }
    }
}