﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Accord.Controls;
using Accord.Imaging;
using Accord.Imaging.Filters;
using Accord.MachineLearning;
using Image = Accord.Imaging.Image;

namespace ImageAnalysis_CLI
{
    public class Program
    {
        private const string ImagePath = @"C:\Users\Raven\Desktop\test.jpg";
        private static readonly Color PointColor = Color.FromArgb(255, 255, 0);

        private static void Surf()
        {
            var bitmap = new Bitmap(ImagePath);
            var surf = new SpeededUpRobustFeaturesDetector();
            var points = surf.Transform(bitmap);

            var outputBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            using (var graphics = Graphics.FromImage(outputBitmap))
            {
                graphics.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height));

                foreach (var point in points)
                    graphics.DrawRectangle(new Pen(PointColor), (int)point.X - 1, (int)point.Y - 1, 3, 3);
            }

            ImageBox.Show(outputBitmap, outputBitmap.Width, outputBitmap.Height);
        }

        private static void Ransac()
        {
            const string image1Path = @"C:\Users\Raven\Desktop\TestPhotos\img1.jpg";
            const string image2Path = @"C:\Users\Raven\Desktop\TestPhotos\img2.jpg";

            var image1Bitmap = new Bitmap(image1Path);
            var image2Bitmap = new Bitmap(image2Path);

            var surf = new SpeededUpRobustFeaturesDetector();
            var image1Points = surf.Transform(image1Bitmap);
            var image2Points = surf.Transform(image2Bitmap);

            var matcher = new KNearestNeighborMatching(25);
            var matches = matcher.Match(image1Points, image2Points);
            
            var ransac = new RansacHomographyEstimator(0.1, 0.99);
            var homographyMatrix = ransac.Estimate(matches);

            var blend = new Blend(homographyMatrix, image1Bitmap);
            var resultBitmap = blend.Apply(image2Bitmap);

            ImageBox.Show(resultBitmap, PictureBoxSizeMode.Zoom, 640, 480);

            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        public static void Main(string[] _args)
        {
            Ransac();
        }
    }
}